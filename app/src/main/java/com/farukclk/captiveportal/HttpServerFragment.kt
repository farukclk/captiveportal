package com.farukclk.captiveportal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class HttpServerFragment : Fragment() {

    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_http_server, container, false)
        textView  = view.findViewById(R.id.htmlLogsTextView)

        return view
    }

     fun updateText(text: String) {
        activity?.runOnUiThread {
            textView.append("$text\n")
        }

    }

}