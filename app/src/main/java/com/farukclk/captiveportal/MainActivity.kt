package com.farukclk.captiveportal


import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

import java.io.File
import java.io.FileOutputStream
import java.io.IOException

import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream


class MyFragmentAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int {
        return 3 // Toplam fragment sayısı
    }



    override fun createFragment(position: Int): Fragment {
        // Her bir fragmentı oluştur ve döndür
        return when (position) {
            0 -> MainFragment()
            1 -> HttpServerFragment()  // http logs
            2 -> BettercapFragment()  // bettercap logs
            else -> throw IllegalArgumentException("Invalid position")
        }
    }
}





class MainActivity : AppCompatActivity() {
    private lateinit var viewPager: ViewPager2


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // gerekli dosyalari files klasorune cikart
        val bettercap = filesDir.absolutePath + "/bettercap"

        if (!File(bettercap).exists()) {
            unzipFromAssets(applicationContext, "a.zip")
        }

        viewPager = findViewById(R.id.view_pager)

        val adapter = MyFragmentAdapter(this)
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 3




        val tabLayout: TabLayout = findViewById(R.id.tab_layout)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = "Main"
                1 -> tab.text = "Http Logs"
                2 -> tab.text = "Bettercap"
            }
        }.attach()


    }



    // gerkeli dosyalari zipten cikart
    private fun unzipFromAssets(context: Context, zipFilename: String?) {
        println("unzip...")
        try {
            val assetManager = context.assets
            val inputStream = zipFilename?.let { assetManager.open(it) }
            val zipInputStream = ZipInputStream(inputStream)
            var zipEntry: ZipEntry?

            while (zipInputStream.nextEntry.also { zipEntry = it } != null) {
                val fileName = zipEntry?.name
                val outFile = File(context.filesDir, fileName)

                // Dizin oluşturma
                if (zipEntry?.isDirectory == true) {
                    outFile.mkdirs()
                } else {
                    // Dosya oluşturma
                    val fileOutputStream = FileOutputStream(outFile)
                    val buffer = ByteArray(1024)
                    var length: Int
                    while (zipInputStream.read(buffer).also { length = it } > 0) {
                        fileOutputStream.write(buffer, 0, length)
                    }
                    fileOutputStream.close()
                }
            }

            zipInputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }



        // Dosyaya çalışma izni verme
        val permissionCommand1 = "chmod +x " + filesDir.absolutePath + "/bettercap"
        val permissionProcess1 = Runtime.getRuntime().exec(permissionCommand1)
        permissionProcess1.waitFor()
        val permissionCommand2 = "chmod +x " + filesDir.absolutePath + "/http_server"
        val permissionProcess2 = Runtime.getRuntime().exec(permissionCommand2)
        permissionProcess2.waitFor()

    }



    override fun onDestroy() {
        super.onDestroy()
    }


}