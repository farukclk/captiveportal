package com.farukclk.captiveportal

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName

import android.content.Context
import android.content.Intent

import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader


class MainFragment : Fragment() {

    private var wifiManager: WifiManager? = null
    private var buttonState: Boolean = true  // true:Start , false:Stop
    private val REQUEST_CODE_PICK_FOLDER = 123
    private var htmlDirectory = ""
    private var filesDirectory = ""
    private var wlan : String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    @SuppressLint("UseSwitchCompatOrMaterialCode")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_main, container, false)
        wifiManager = requireContext().getSystemService(AppCompatActivity.WIFI_SERVICE) as WifiManager

        val switch1 : Switch = view.findViewById(R.id.switch5)
        val switch2 : Switch = view.findViewById(R.id.switch6)
        val htmlButton: Button = view.findViewById(R.id.buttonHtml)
        val htmlEditText: EditText = view.findViewById(R.id.editTextHtmlPath)
        val startButton = view.findViewById<Button>(R.id.button2)


        htmlDirectory = requireContext().filesDir.absolutePath + "/html"
        filesDirectory =   requireActivity().filesDir.absolutePath





        htmlEditText.setText(htmlDirectory)




        switch2.setOnCheckedChangeListener() { _, isChecked ->
            htmlButton.isEnabled = isChecked
            htmlEditText.isEnabled = isChecked
        }


        // html dosyalarinin bulundugu dizini sec
        htmlButton.setOnClickListener() {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
            startActivityForResult(intent, REQUEST_CODE_PICK_FOLDER)
        }



        switch1.setOnCheckedChangeListener { _, isChecked ->
            // Birinci switch'in durumunu kontrol et
            switch2.isEnabled = isChecked


            htmlButton.isEnabled = isChecked && switch2.isChecked
            htmlEditText.isEnabled = isChecked && switch2.isChecked

        }







        startButton.setOnClickListener {


            if (!buttonState) {
                stop(requireContext())
                buttonState = true
                startButton.text = "start"
                return@setOnClickListener
            }


            // html klasorunu güncelle
            htmlDirectory = requireContext().filesDir.absolutePath + "/html"
            if (switch2.isChecked && switch2.isEnabled) {
                htmlDirectory = htmlEditText.text.toString()
            }

            if (! File(htmlDirectory).isDirectory) {
                Toast.makeText(requireContext(), "Html directory is not found", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            buttonState = false
            startButton.text = "stop"


            val a = Thread {

                // ucak modunu kapat, wifi yi kapat, mobil veriyi ac
                system(" settings put global airplane_mode_on 0;" +
                        "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false;" +
                        "svc wifi disable;" +
                        "svc data enable")




                // hotspotu açtır
                if (! isHotspotEnabled(requireContext())) {
                    enableHotspot(view.context)

                    while (!isHotspotEnabled(requireContext())) {
                        Thread.sleep(1000)
                    }
                }

                wlan =  getHotspotInterface()

                if (wlan == null) {
                    println("Hata: Ağ kartı bulunamadı [!]")
                    startButton.text = "Start"
                    buttonState = true
                    return@Thread
                }


                // internet ersimini kes
                system("iptables -A FORWARD -i $wlan -j DROP")

                startHttpServer(htmlDirectory!!)
                executeBettercap()
                startButton.isEnabled = true 

            }
            a.start()
        }

        return view
    }





    private fun executeBettercap() {

        val a = Thread {

            val fragment = requireActivity().supportFragmentManager.fragments[2] as? BettercapFragment


            val process = Runtime.getRuntime().exec(arrayOf("su", "-c",  "LD_LIBRARY_PATH=$filesDirectory", "$filesDirectory/bettercap", "-iface", wlan, "-eval","'set dns.spoof.domains *;dns.spoof on;net.sniff on'"))
            //     val process = Runtime.getRuntime().exec(arrayOf("su", "-c",  "LD_LIBRARY_PATH=$filesDirectory", "filesDirectory/bettercap", "-iface", "wlan2","-eval", "help"))
            val reader = BufferedReader(InputStreamReader(process.inputStream))
            var line: String?
            while (reader.readLine().also { line = it } != null) {

                fragment?.updateText("$line")

            }
            // İşlem tamamlandıktan sonra process'i sonlandır
            process.waitFor()

        }
        a.start()
    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_PICK_FOLDER && resultCode == Activity.RESULT_OK) {
            val selectedFolderUri = data?.data?.path.toString()  // /tree/primary:Android/media

            val htmlDirectoryEditText: EditText = requireView().findViewById(R.id.editTextHtmlPath)

            val newPath= selectedFolderUri.replaceFirst("/tree/primary:", "/sdcard/")

            htmlDirectoryEditText.setText(newPath)

        }
    }



    private fun startHttpServer(htmlPath : String) {

        val a = Thread {
            var fragment = requireActivity().supportFragmentManager.fragments[1] as? HttpServerFragment

            var process = Runtime.getRuntime().exec(arrayOf("su", "-c",  "$filesDirectory/http_server", htmlPath, "80"))

            val reader = BufferedReader(InputStreamReader(process.inputStream))
            var line: String?
            while (reader.readLine().also { line = it } != null) {


                fragment?.updateText("$line")

            }
            // İşlem tamamlandıktan sonra process'i sonlandır
            process.waitFor()

        }
        a.start()

    }




    private fun enableHotspot(context: Context) {
        val intent = Intent(Intent.ACTION_MAIN, null)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val cn = ComponentName("com.android.settings", "com.android.settings.TetherSettings")
        intent.component = cn
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }



    override fun onDestroy() {
        super.onDestroy()
        if (!buttonState) {  // button.text == "stop"
            stop(requireContext())
        }
    }



    companion object {
        fun isHotspotEnabled(context: Context): Boolean {
            val wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
            try {
                val method = wifiManager.javaClass.getMethod("isWifiApEnabled")
                return method.invoke(wifiManager) as Boolean
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return false
        }

        // execute shell commands with root
        fun system(command : String) {
            val process = Runtime.getRuntime()
                .exec(arrayOf("su", "-c", command))
            process.waitFor()
        }

        fun stop (context: Context) {

            val process = Thread {

                var command = ""

                val wlan = getHotspotInterface()
                if (wlan != null)
                    command =  "iptables -D FORWARD -i $wlan -j DROP;"

                command += "settings put global airplane_mode_on 1;" +
                        "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true;" +
                        "pkill http_server;" +
                        "pkill bettercap;"

                system(command)
            }
            process.start()
        }


        // return network interface card
        fun getHotspotInterface(): String? {

            var interfaceName : String? = null
            try {
                val process = Runtime.getRuntime().exec("ip link show")
                val reader = BufferedReader(InputStreamReader(process.inputStream))
                var line: String?
                while (reader.readLine().also { line = it } != null) {
                    if (line!!.contains("wlan") && line!!.contains("BROADCAST,MULTICAST,UP,LOWER_UP")) { // Burada, WiFi arabirimlerini filtreleyebilirsiniz
                        val parts = line!!.split(" ")
                        interfaceName = parts[1].substring(0, parts[1].length - 1)
                        break
                    }
                }
                reader.close()
                process.waitFor()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return interfaceName
        }
    }

}