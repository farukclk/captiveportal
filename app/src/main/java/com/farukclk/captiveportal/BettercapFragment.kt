package com.farukclk.captiveportal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class BettercapFragment : Fragment() {

    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_bettercap, container, false)
        textView  = view.findViewById(R.id.bettercapTextView)


        return view
    }

    public fun updateText(text: String) {
        activity?.runOnUiThread {
            textView.append("$text\n")
        }

    }

}